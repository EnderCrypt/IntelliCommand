/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.interceptor;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.exception.execution.InterceptorException;


/**
 * class that holds all {@link Interceptor}'s
 * 
 * @author EnderCrypt
 */
public class Interceptors implements Iterable<Interceptor>
{
	private List<Interceptor> interceptors = new ArrayList<>();
	
	/**
	 * adds a new {@link Interceptor} to be ran before each command
	 * 
	 * @param interceptor
	 */
	public void register(Interceptor interceptor)
	{
		interceptors.add(interceptor);
	}
	
	/**
	 * removes an {@link Interceptor}
	 * 
	 * @param interceptor
	 * @return true if the interceptor existed and was removed
	 */
	public boolean remove(Interceptor interceptor)
	{
		return interceptors.remove(interceptor);
	}
	
	/**
	 * runs a command and bundle through all {@link Interceptor}'s
	 * 
	 * @param command
	 * @param bundle
	 */
	public void intercept(Bundle bundle, Command command, Object[] arguments) throws InterceptorException
	{
		try
		{
			for (Interceptor interceptor : interceptors)
			{
				interceptor.intercept(bundle, command, arguments);
			}
		}
		catch (Exception e)
		{
			throw new InterceptorException(command, arguments, e);
		}
	}
	
	@Override
	public Iterator<Interceptor> iterator()
	{
		return Collections.unmodifiableList(interceptors).iterator();
	}
	
	@Override
	public String toString()
	{
		return interceptors.toString();
	}
}
