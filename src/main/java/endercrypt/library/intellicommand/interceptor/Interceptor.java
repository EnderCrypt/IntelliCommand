/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.interceptor;


import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;


/**
 * Interceptor's need to be stored inside {@link IntelliCommand}'s
 * {@link Interceptors} class. interceptor's are run automatically just before a
 * command method gets called and can thus be used to for example modify the
 * bundle based on the command metadata, detect extra annotations, or even
 * interrupt the whole flow of execution by throwing an unchecked exception.
 * 
 * @author EnderCrypt
 */
public interface Interceptor
{
	/**
	 * intercept method for running custom code before executing a command
	 * method
	 * 
	 * @param command
	 *            about to be execute
	 * @param commandArguments
	 *            the arguments queued to get passed along to the command
	 * @param bundle
	 *            passed along with this command
	 */
	public void intercept(Bundle bundle, Command command, Object[] arguments);
}
