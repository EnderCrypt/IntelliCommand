/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand;


import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Commands;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;
import endercrypt.library.intellicommand.execution.CommandExecutionReport;
import endercrypt.library.intellicommand.execution.CommandExecutor;
import endercrypt.library.intellicommand.interceptor.Interceptors;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandParserFactory;


/**
 * main class for managing commands you can register commands by doing
 * {@link #commands()} and then calling the appropriate register. custom
 * argument types can be created by creating {@link ArgMapper}'s and registering
 * them into {@link #mappers()}.
 * 
 * once some commands have been added, they can be executed by running a command
 * through {@link IntelliCommand#execute(String)}
 * 
 * @author EnderCrypt
 */
public class IntelliCommand
{
	private final CommandParserFactory commandParserFactory = new CommandParserFactory();
	private final Commands commands = new Commands();
	private final Mappers mappers = new Mappers();
	private final Interceptors interceptors = new Interceptors();
	
	/**
	 * @return the {@link CommandParserFactory} used for parsing commands.
	 */
	public CommandParserFactory commandParserFactory()
	{
		return commandParserFactory;
	}
	
	/**
	 * @return the container object that holds all {@link Command}'s.
	 */
	public Commands commands()
	{
		return commands;
	}
	
	/**
	 * @return the container object that holds all {@link ArgMapper}'s.
	 */
	public Mappers mappers()
	{
		return mappers;
	}
	
	/**
	 * @return the container object that holds all {@link Interceptor}'s.
	 */
	public Interceptors interceptors()
	{
		return interceptors;
	}
	
	/**
	 * attempts to find and activate a command from a class thats been
	 * previously added through {@code commands().register(Object)}.
	 * 
	 * @param commandString
	 *            to match into a command to execute
	 * 			
	 * @return a {@link CommandExecutionReport} about the command that was
	 *         executed
	 * 
	 * @throws UnderlyingCommandException
	 *             if the executed command threw an exception
	 * @throws CommandNotFoundException
	 *             if no matching commands were found
	 * @throws CommandArgumentMismatchException
	 *             if 1 or more matching commands were found, but arguments did
	 *             not match
	 * @throws MalformedCommandException
	 *             if the arguments were broken, such as by a non-closed quote
	 */
	public CommandExecutionReport execute(String commandString) throws UnderlyingCommandException, CommandNotFoundException, CommandArgumentMismatchException, MalformedCommandException
	{
		return execute(commandString, new Bundle());
	}
	
	/**
	 * attempts to find and activate a command from a class thats been
	 * previously added through {@code commands().register(Object)}, a bundle of
	 * objects should be included, these will be passed on to command methods
	 * with the {@code @Include} annotation.
	 * 
	 * @param commandString
	 *            to match into a command to execute
	 * @param bundle
	 *            the bundle to include, these will be used to supply
	 *            {@code @Include} parameters in the command method
	 * 			
	 * @return a {@link CommandExecutionReport} about the command that was
	 *         executed
	 * 
	 * @throws UnderlyingIntelliException
	 *             if the command method threw an exception
	 * @throws IntelliCommandNotFound
	 *             if no matching commands exists
	 * @throws IntelliCommandArgumentMismatch
	 *             if 1 or more matching commands were found, but arguments
	 *             didn't match
	 * @throws MalformedCommandException
	 *             if the arguments were broken, such as by a non-closed quote
	 * 			
	 * @see endercrypt.library.intellicommand.annotation.Include
	 */
	public CommandExecutionReport execute(String commandString, Bundle bundle) throws UnderlyingCommandException, CommandNotFoundException, CommandArgumentMismatchException, MalformedCommandException
	{
		return CommandExecutor.perform(this, commandString, bundle);
	}
}
