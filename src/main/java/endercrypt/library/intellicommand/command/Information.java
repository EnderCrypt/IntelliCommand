/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.command;


import java.lang.reflect.Method;
import java.util.Optional;
import java.util.function.Function;

import endercrypt.library.intellicommand.annotation.CommandInformation;


/**
 * information object, containing all the gathered information from an command's
 * {@link CommandInformation} annotation if such annotation wasn't present
 * {@link #isAvailable()} will return false, but getters will still be
 * functional (although they'll all be guaranteed to return null)
 * 
 * @author EnderCrypt
 */
public class Information
{
	/**
	 * creates an information object based on {@link CommandInformation}
	 * attached to a method and its class
	 * 
	 * @param method
	 *            to check for {@link CommandInformation} on
	 * @return an {@link Information} object representing this command method
	 */
	public static Information resolve(Method method)
	{
		String name = resolveInformation(CommandInformation::name, method);
		String category = resolveInformation(CommandInformation::category, method);
		String description = resolveInformation(CommandInformation::description, method);
		String example = resolveInformation(CommandInformation::example, method);
		String author = resolveInformation(CommandInformation::author, method);
		return new Information(name, description, category, example, author);
	}
	
	private static String resolveInformation(Function<CommandInformation, String> getter, Method method)
	{
		return resolveInformation(getter,
			method.getDeclaringClass().getDeclaredAnnotation(CommandInformation.class),
			method.getDeclaredAnnotation(CommandInformation.class));
	}
	
	private static String resolveInformation(Function<CommandInformation, String> getter, CommandInformation... annotations)
	{
		String value = null;
		for (CommandInformation annotation : annotations)
		{
			if (annotation != null)
			{
				String annotationValue = getter.apply(annotation);
				if (annotationValue.equals("") == false)
				{
					value = annotationValue;
				}
			}
		}
		return value;
	}
	
	private final String name;
	private final String description;
	private final String category;
	private final String example;
	private final String author;
	
	public Information(String name, String description, String category, String example, String author)
	{
		this.name = name;
		this.description = description;
		this.category = category;
		this.example = example;
		this.author = author;
	}
	
	/**
	 * gets the name from the {@link CommandInformation} annotation
	 * 
	 * @return optional name
	 */
	public Optional<String> getName()
	{
		return Optional.ofNullable(name);
	}
	
	/**
	 * gets the description from the {@link CommandInformation} annotation
	 * 
	 * @return optional description
	 */
	public Optional<String> getDescription()
	{
		return Optional.ofNullable(description);
	}
	
	/**
	 * gets the category from the {@link CommandInformation} annotation
	 * 
	 * @return optional category
	 */
	public Optional<String> getCategory()
	{
		return Optional.ofNullable(category);
	}
	
	/**
	 * gets the example from the {@link CommandInformation} annotation
	 * 
	 * @return optional example
	 */
	public Optional<String> getExample()
	{
		return Optional.ofNullable(example);
	}
	
	/**
	 * gets the author from the {@link CommandInformation} annotation
	 * 
	 * @return optional author
	 */
	public Optional<String> getAuthor()
	{
		return Optional.ofNullable(author);
	}
}
