/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.command;


import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Priority;
import endercrypt.library.intellicommand.exception.IntelliCommandBuildException;
import endercrypt.library.intellicommand.exception.IntelliCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;
import endercrypt.library.intellicommand.signature.SignatureArray;
import endercrypt.library.intellicommand.signature.SignatureCollection;
import endercrypt.library.intellicommand.signature.pieces.VariablePiece;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


/**
 * class representing a command based on a {@link Method} found on an object fed
 * into {@link Commands#register(Object)}
 *
 * @author EnderCrypt
 */
public class Command implements Comparable<Command>
{
	/**
	 * performs a basic check to see if a command looks as if it should be a
	 * command
	 * 
	 * @param method
	 *     to check
	 * @return true if this method should be treated as a command
	 */
	public static boolean isCommandMethod(Method method)
	{
		return method.isAnnotationPresent(CommandSignature.class);
	}
	
	/**
	 * constructs a new command object based on the object and method provided
	 * {@link #generateCommand(Object object, Method method)}
	 *
	 * @param object
	 * @param method
	 *     from object
	 * @return a command instance based on the method
	 * @throws IntelliCommandException
	 *     if the method is not a valid command
	 */
	public static Command constructCommand(Object object, Method method)
	{
		// class
		Class<?> methodClass = object.getClass();
		
		// check that method and object belong to eachother
		if (method.getDeclaringClass().equals(methodClass) == false)
		{
			throw new IntelliCommandBuildException(methodClass, method, "object and method does not beling togheter");
		}
		
		// command signature
		SignatureCollection signatures = new SignatureCollection(methodClass, method);
		
		// priority
		int priority = Optional.ofNullable(method.getAnnotation(Priority.class)).map(Priority::value).orElse(0);
		
		// information
		Information information = Information.resolve(method);
		
		// construct command
		return new Command(object, signatures, method, priority, information);
	}
	
	private Object object;
	private SignatureCollection signatures;
	private Method method;
	private int priority;
	private Information information;
	
	public Command(Object object, SignatureCollection signatures, Method method, int priority, Information information)
	{
		this.object = Objects.requireNonNull(object, "object");
		this.signatures = Objects.requireNonNull(signatures, "signatures");
		this.method = Objects.requireNonNull(method, "method");
		this.priority = priority;
		this.information = Objects.requireNonNull(information, "information");
		
		// verify command parameters
		List<VariablePiece> variableExpectations = VariablePiece.resolve(getParameters());
		List<VariablePiece> variables = getPrimarySignature().getVariables();
		
		if (variableExpectations.containsAll(variables) == false || variables.containsAll(variableExpectations) == false)
		{
			throw new IntelliCommandBuildException(object.getClass(), method, "\"" + getPrimarySignature().asText() + "\" does not match parameters " + VariablePiece.toNames(variableExpectations));
		}
	}
	
	/**
	 * @return the primary signature of this command
	 */
	public SignatureCollection getSignatures()
	{
		return signatures;
	}
	
	/**
	 * @return the primary signature of this command
	 */
	public SignatureArray getPrimarySignature()
	{
		return getSignatures().getPrimary();
	}
	
	/**
	 * @return the method this class is watching over
	 */
	public Method getMethod()
	{
		return method;
	}
	
	/**
	 * @return the parameters attached to the command method available using
	 * {@link #getMethod()}
	 */
	public Parameter[] getParameters()
	{
		return getMethod().getParameters();
	}
	
	/**
	 * gives the information object, generated from the
	 * {@link CommandInformation} annotation
	 * 
	 * @return information
	 */
	public Information getInformation()
	{
		return information;
	}
	
	/**
	 * gives the priority of this command for being executed over other commands
	 * incase of an overload scenario, the default is 0 higher numbers give
	 * higher priority lower numbers give lower priority priority can be set on
	 * a command method using the {@link Priority} annotation
	 *
	 * @return priority value
	 */
	public int getPriority()
	{
		return priority;
	}
	
	/**
	 * attempts to execute this command method with an array of object args,
	 * typically called with the return value from obtainArguments
	 *
	 * @param commandArguments
	 *     the arguments to activate the command method with these need to be
	 *     the same class or a subclass of the method parameters
	 * @throws UnderlyingIntelliException
	 *     the command method threw an exception
	 * @throws IntelliCommandException
	 *     the command method itself was inaccessible, possibly private method
	 *     or other access restrictions
	 * @throws IllegalArgumentException
	 *     the arguments did not fit the command method
	 * 
	 */
	public Object execute(Object... commandArguments) throws UnderlyingCommandException
	{
		try
		{
			return method.invoke(object, commandArguments);
		}
		catch (InvocationTargetException e)
		{
			throw new UnderlyingCommandException(this, commandArguments, e.getCause());
		}
		catch (IllegalAccessException e)
		{
			throw new IntelliCommandException("command method innaccessible", e);
		}
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Command other = (Command) obj;
		if (method == null)
		{
			if (other.method != null) return false;
		}
		else if (!method.equals(other.method)) return false;
		return true;
	}
	
	@Override
	public int compareTo(Command other)
	{
		return Integer.compare(other.priority, priority);
	}
	
	@Override
	public String toString()
	{
		return getPrimarySignature().toString();
	}
}
