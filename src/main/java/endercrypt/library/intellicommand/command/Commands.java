/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.command;


import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.utility.IntelliCommandUtility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * container for all {@link Command}'s available
 * 
 * @author EnderCrypt
 */
public class Commands implements Iterable<Command>
{
	private final List<Command> commands = new ArrayList<>();
	
	/**
	 * @return the amount of commands available
	 */
	public int count()
	{
		return commands.size();
	}
	
	/**
	 * registers an object to receive commands, for a class to be accepted it
	 * needs to have {@link CommandSignature} as a class annotation and for each
	 * method that should be a command
	 * 
	 * @param object
	 *            objects with command methods (using {@link CommandSignature})
	 */
	public List<Command> register(Object object)
	{
		// class
		Class<?> objectClass = object.getClass();
		String objectClassName = objectClass.getSimpleName();
		
		// check if class has CommandSignature annotation
		if (objectClass.isAnnotationPresent(CommandSignature.class) == false)
		{
			throw new IllegalArgumentException(objectClassName + " missing annotation " + CommandSignature.class.getSimpleName());
		}
		
		// generate commands
		List<Command> freshCommands = Arrays.stream(objectClass.getDeclaredMethods())
			.filter(Command::isCommandMethod)
			.map(method -> Command.constructCommand(object, method))
			.collect(Collectors.toUnmodifiableList());
		
		// add
		commands.addAll(freshCommands);
		Collections.sort(commands);
		
		return freshCommands;
	}
	
	/**
	 * returns a set of available commands with the specified category
	 * 
	 * @param category
	 * @return {@link Set} of commands
	 */
	public Set<Command> getCommands(String category)
	{
		Set<Command> result = new HashSet<>();
		for (Command command : this)
		{
			command.getInformation().getCategory().ifPresent(commandCategory -> {
				if (category.equalsIgnoreCase(commandCategory))
				{
					result.add(command);
				}
			});
		}
		return result;
	}
	
	/**
	 * returns a set of all available categories as defined by
	 * {@link endercrypt.library.intellicommand.annotation.CommandInformation}
	 * annotation
	 * 
	 * @return
	 */
	public Set<String> getCategories()
	{
		Set<String> categories = new HashSet<>();
		for (Command command : this)
		{
			command.getInformation().getCategory().ifPresent(category -> categories.add(IntelliCommandUtility.capitalize(category)));
		}
		return categories;
	}
	
	@Override
	public Iterator<Command> iterator()
	{
		return Collections.unmodifiableList(commands).iterator();
	}
	
	@Override
	public String toString()
	{
		return commands.stream().map(Command::toString).collect(Collectors.joining("\n"));
	}
}
