/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser;


import java.util.ArrayList;
import java.util.List;

import endercrypt.library.intellicommand.parser.module.ParserModule;
import endercrypt.library.intellicommand.parser.module.modules.BundleParser;
import endercrypt.library.intellicommand.parser.module.modules.LeftoverParser;
import endercrypt.library.intellicommand.parser.module.modules.ParameterParamParser;
import endercrypt.library.intellicommand.parser.module.modules.SignatureChecker;


/**
 * factory used to construct {@link CommandParser}'s any {@link ParserModule}
 * should be added in here using {@link #addModule(ParserModule)} before a
 * command string is parsed
 * 
 * @author EnderCrypt
 */
public class CommandParserFactory
{
	private List<ParserModule> modules = new ArrayList<>();
	
	public CommandParserFactory()
	{
		addModule(new SignatureChecker());
		addModule(new BundleParser());
		addModule(new ParameterParamParser());
		addModule(new LeftoverParser());
	}
	
	/**
	 * adds a {@link ParserModule} to be used on all future command parsings
	 * 
	 * @param parserModule
	 *            implementation to add
	 */
	public void addModule(ParserModule parserModule)
	{
		modules.add(parserModule);
	}
	
	/**
	 * removes a {@link ParserModule}
	 * 
	 * @param parserModule
	 *            implementation to remove
	 * @return true if the {@link ParserModule} was found and removed
	 */
	public boolean removeModule(ParserModule parserModule)
	{
		return modules.remove(parserModule);
	}
	
	/**
	 * generates a new {@link CommandParser} using any available
	 * {@link ParserModule}'s
	 * 
	 * @return a new {@link CommandParser}
	 */
	public CommandParser build()
	{
		return new CommandParser(modules);
	}
	
}
