/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module.modules;


import java.util.List;

import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.bundle.BundleEntry;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.Expectation;
import endercrypt.library.intellicommand.parser.module.SequentialAnnotationParserModule;
import endercrypt.library.intellicommand.signature.SignatureArray;


/**
 * this {@link ParserModule} attempts to fill in all {@link Expectation}'s with
 * the {@link Include} annotation by looking in the supplied bundle for the
 * matching key and then filling the expectation
 * 
 * @author EnderCrypt
 */
public class BundleParser extends SequentialAnnotationParserModule<Include>
{
	public BundleParser()
	{
		super(Include.class);
	}
	
	@Override
	public void executeAnnotation(Command command, SignatureArray signature, Expectation expectation, Include annotation, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException
	{
		String key = annotation.value();
		if (bundle.has(key) == false)
		{
			throw new CommandRejectedException("bundle lacks the command method specified key: " + key);
		}
		BundleEntry bundleEntry = bundle.get(key);
		expectation.fill(bundleEntry.getBundleClass(), bundleEntry.getSupplier());
	}
}
