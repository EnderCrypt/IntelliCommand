/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module.modules;


import endercrypt.library.intellicommand.annotation.CommandParam;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.mapper.MapperConversionFail;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.expectation.Expectation;
import endercrypt.library.intellicommand.parser.module.SequentialAnnotationParserModule;
import endercrypt.library.intellicommand.signature.Piece;
import endercrypt.library.intellicommand.signature.SignatureArray;
import endercrypt.library.intellicommand.signature.pieces.VariablePiece;
import endercrypt.library.intellicommand.utility.IntelliCommandPrimitiveMapper;

import java.util.List;


/**
 * {@link ParserModule} that goes through and fills in any {@link Expectation}'s
 * using {@link CommandParam}
 * 
 * @author EnderCrypt
 */
public class ParameterParamParser extends SequentialAnnotationParserModule<CommandParam>
{
	public ParameterParamParser()
	{
		super(CommandParam.class);
	}
	
	@Override
	public void executeAnnotation(Command command, SignatureArray signature, Expectation expectation, CommandParam annotation, List<String> args, Mappers mappers, Bundle bundle)
	{
		Class<?> parameterClass = expectation.getType();
		
		String parameterVariable = annotation.value();
		
		for (int i = 0; i < Math.min(signature.length(), args.size()); i++)
		{
			Piece signaturePiece = signature.get(i);
			String commandValue = args.get(i);
			
			if (signaturePiece instanceof VariablePiece)
			{
				VariablePiece variablePiece = (VariablePiece) signaturePiece;
				if (variablePiece.getName().contentEquals(parameterVariable))
				{
					try
					{
						Object object = mappers.map(bundle, parameterClass, commandValue);
						if (IntelliCommandPrimitiveMapper.outmapPrimitiveClasses(parameterClass).isAssignableFrom(object.getClass()))
						{
							expectation.fill(object);
						}
					}
					catch (MapperConversionFail e)
					{
						continue;
					}
					return;
				}
			}
		}
	}
}
