/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module.modules;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import endercrypt.library.intellicommand.annotation.Leftovers;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.exception.IntelliCommandException;
import endercrypt.library.intellicommand.mapper.MapperConversionFail;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.Expectation;
import endercrypt.library.intellicommand.parser.module.SequentialAnnotationParserModule;
import endercrypt.library.intellicommand.signature.SignatureArray;
import endercrypt.library.intellicommand.utility.IntelliCommandPrimitiveMapper;


/**
 * {@link ParserModule} that fills in any {@link Expectation}'s signaled by
 * {@link Leftovers} parameter
 * 
 * @author EnderCrypt
 */
public class LeftoverParser extends SequentialAnnotationParserModule<Leftovers>
{
	public LeftoverParser()
	{
		super(Leftovers.class);
	}
	
	@Override
	public void executeAnnotation(Command command, SignatureArray signature, Expectation expectation, Leftovers annotation, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException
	{
		Class<?> parameterClass = expectation.getType();
		if (parameterClass.isArray())
		{
			throw new IntelliCommandException(Leftovers.class.getSimpleName() + " does not support arrays");
		}
		String leftoverString = IntStream.range(signature.length(), args.size())
			.mapToObj(args::get)
			.collect(Collectors.joining(" "));
		
		if (leftoverString.isEmpty() && annotation.requireFilled())
		{
			throw new CommandRejectedException("@" + Leftovers.class.getSimpleName() + " has no text to parse");
		}
		else
		{
			try
			{
				Object object = mappers.map(bundle, parameterClass, leftoverString);
				if (IntelliCommandPrimitiveMapper.outmapPrimitiveClasses(parameterClass).isAssignableFrom(object.getClass()))
				{
					expectation.fill(object);
				}
			}
			catch (MapperConversionFail e)
			{
				throw new CommandRejectedException("@" + Leftovers.class.getSimpleName() + " failed to parse for " + parameterClass);
			}
		}
	}
}
