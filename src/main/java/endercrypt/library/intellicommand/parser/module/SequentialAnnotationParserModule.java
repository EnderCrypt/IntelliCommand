/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module;


import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.List;

import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.Expectation;
import endercrypt.library.intellicommand.signature.SignatureArray;


/**
 * this class is practically the same as {@link SequentialParserModule} except
 * it only feeds the expectations that have a specific annotation on it to
 * {@link #executeAnnotation(Command, Expectation, Annotation, String[], Mappers, Bundle)}
 * 
 * @author EnderCrypt
 */
public abstract class SequentialAnnotationParserModule<T extends Annotation> extends SequentialParserModule
{
	private Class<T> annotationClass;
	
	/**
	 * @param annotationClass
	 *            the annotation to look for in each parameter and then call
	 *            {@link #executeAnnotation(Command, Expectation, Annotation, args, Mappers, Bundle)}
	 *            with
	 */
	protected SequentialAnnotationParserModule(Class<T> annotationClass)
	{
		this.annotationClass = annotationClass;
	}
	
	@Override
	public final void executeExpectation(Command command, SignatureArray signature, Expectation expectation, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException
	{
		Parameter parameter = expectation.getParameter();
		T annotation = parameter.getAnnotation(annotationClass);
		if (annotation != null)
		{
			executeAnnotation(command, signature, expectation, annotation, args, mappers, bundle);
		}
	}
	
	/**
	 * runs for each unmet {@link Expectation} with the supplied annotation
	 * 
	 * @param command
	 *            the command thats currently being evaluated for these
	 *            {@link MethodExpecations} and cycle of parsing
	 * @param signature
	 *            the {@link SignatureArray} of the command, to check for in
	 *            this parse iteration
	 * @param expectation
	 *            the expectation that should be tested and supplied if
	 *            appropriate
	 * @param annotation
	 *            the annotation instance found in the parameter/expectation
	 * @param args
	 *            the text command that
	 *            {@link endercrypt.library.intellicommand.IntelliCommand#execute(String)}
	 *            was called with
	 * @param mappers
	 *            the {@link Mappers} available for the parser module to use
	 * @param bundle
	 *            the {@link Bundle} available for the parser module to use
	 * @throws CommandRejectedException
	 *             if the parser rejected the arguments for this command
	 */
	public abstract void executeAnnotation(Command command, SignatureArray signature, Expectation expectation, T annotation, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException;
	
}
