/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module;


import java.util.List;

import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.MethodExpecations;
import endercrypt.library.intellicommand.signature.SignatureArray;


/**
 * a parser module is responsible of looping through the
 * {@link MethodExpecations} which contains all the parameters on the command
 * method that {@link endercrypt.library.intellicommand.IntelliCommand} is
 * currently going through when attempting to evaluate a command
 * 
 * each module should attempt to fill in as many reasonable expectations as they
 * can the command will be valid and executed if all expectations are filled
 * 
 * for a {@link ParserModule} to be used, it has to be added into
 * {@link CommandParserFactory#addModule(ParserModule)}
 * 
 * you can force a command to be declined by throwing
 * {@link CommandRejectedException}
 * 
 * @author EnderCrypt
 */
public interface ParserModule
{
	/**
	 * attempts to cycle through all {@link MethodExpecations} and fill them to
	 * its best extent
	 * 
	 * @param command
	 *            the command thats currently being evaluated for these
	 *            {@link MethodExpecations} and cycle of parsing
	 * @param signature
	 *            the {@link SignatureArray} of the command, to check for in
	 *            this parse iteration
	 * @param methodExpecations
	 *            the expectations (command method parameters) that the parser
	 *            is hopping to have fulfilled
	 * @param args
	 *            the text command that
	 *            {@link endercrypt.library.intellicommand.IntelliCommand#execute(String)}
	 *            was called with
	 * @param mappers
	 *            the {@link Mappers} available for the parser module to use
	 * @param bundle
	 *            the {@link Bundle} available for the parser module to use
	 * @throws CommandRejectedException
	 *             if the parser rejected the arguments for this command
	 */
	public void execute(Command command, SignatureArray signature, MethodExpecations methodExpecations, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException;
}
