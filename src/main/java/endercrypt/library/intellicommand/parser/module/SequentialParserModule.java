/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module;


import java.util.List;

import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.Expectation;
import endercrypt.library.intellicommand.parser.expectation.MethodExpecations;
import endercrypt.library.intellicommand.signature.SignatureArray;


/**
 * abstract {@link ParserModule} class. this class is intended to make it easier
 * to make parsing module by doing the job of looping through all
 * {@link Expectation} available in {@link MethodExpecations} automatically for
 * you, finding the ones that are unmet and calling
 * {@link #executeExpectation(Command, Expectation, String[], Mappers, Bundle)}
 * with each one
 * 
 * @author EnderCrypt
 */
public abstract class SequentialParserModule implements ParserModule
{
	@Override
	public final void execute(Command command, SignatureArray signature, MethodExpecations methodExpecations, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException
	{
		for (Expectation expectation : iterateUnmet(methodExpecations))
		{
			executeExpectation(command, signature, expectation, args, mappers, bundle);
		}
	}
	
	/**
	 * runs for each unmet {@link Expectation} found while parsing
	 * 
	 * @param command
	 *            the command thats currently being evaluated for these
	 *            {@link MethodExpecations} and cycle of parsing
	 * @param signature
	 *            the {@link SignatureArray} of the command, to check for in
	 *            this parse iteration
	 * @param expectation
	 *            the expectation that should be tested and supplied if
	 *            appropriate
	 * @param args
	 *            the text command that
	 *            {@link endercrypt.library.intellicommand.IntelliCommand#execute(String)}
	 *            was called with
	 * @param mappers
	 *            the {@link Mappers} available for the parser module to use
	 * @param bundle
	 *            the {@link Bundle} available for the parser module to use
	 * @throws CommandRejectedException
	 *             if the parser rejected the arguments for this command
	 */
	public abstract void executeExpectation(Command command, SignatureArray signature, Expectation expectation, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException;
	
	private static Iterable<Expectation> iterateUnmet(MethodExpecations methodExpecations)
	{
		return () -> methodExpecations.getUnmet().iterator();
	}
}
