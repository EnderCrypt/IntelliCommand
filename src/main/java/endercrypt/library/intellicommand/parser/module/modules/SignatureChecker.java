/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.module.modules;


import java.util.List;

import endercrypt.library.intellicommand.annotation.Leftovers;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.mapper.Mappers;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.MethodExpecations;
import endercrypt.library.intellicommand.parser.module.ParserModule;
import endercrypt.library.intellicommand.signature.Piece;
import endercrypt.library.intellicommand.signature.SignatureArray;
import endercrypt.library.intellicommand.signature.pieces.PlainPiece;


/**
 * {@link ParserModule} that checks the command string to see if it matches all
 * {@link PlainPiece}'s available in {@link SignatureArray} and rejects any
 * mismatches by throwing a {@link CommandRejectedException}
 * 
 * @author EnderCrypt
 */
public class SignatureChecker implements ParserModule
{
	@Override
	public void execute(Command command, SignatureArray signature, MethodExpecations methodExpecations, List<String> args, Mappers mappers, Bundle bundle) throws CommandRejectedException
	{
		// check if command is shorter than signature
		if (args.size() < signature.length())
		{
			throw new CommandRejectedException("command is too short");
		}
		
		// check if command overdo's the signature
		boolean hasLeftoverParamater = methodExpecations.getAll().anyMatch(e -> e.getParameter().isAnnotationPresent(Leftovers.class));
		if (hasLeftoverParamater == false && args.size() > signature.length())
		{
			throw new CommandRejectedException("command is too long");
		}
		
		// check if signature and command matches
		for (int i = 0; i < signature.length(); i++)
		{
			Piece piece = signature.get(i);
			if (i >= args.size())
			{
				throw new CommandRejectedException("input command lacks enough arguments to match the entire signature");
			}
			String argument = args.get(i);
			
			if (piece instanceof PlainPiece)
			{
				PlainPiece plainPiece = (PlainPiece) piece;
				String expected = plainPiece.getText();
				if (argument.equalsIgnoreCase(expected) == false)
				{
					throw new CommandRejectedException("argument " + (i + 1) + " expected: \"" + expected + "\" but was: \"" + argument + "\"");
				}
			}
		}
	}
}
