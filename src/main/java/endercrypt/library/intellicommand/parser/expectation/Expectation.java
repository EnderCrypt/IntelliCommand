/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.expectation;


import java.lang.reflect.Parameter;
import java.util.function.Supplier;

import endercrypt.library.intellicommand.utility.IntelliCommandPrimitiveMapper;


/**
 * represents a method {@link Parameter} inside of a command {@link Method} that
 * needs to be fulfilled in order to call it
 * 
 * @author EnderCrypt
 */
public class Expectation
{
	private Parameter parameter;
	private Supplier<?> valueSupplier = null;
	
	public Expectation(Parameter parameter)
	{
		this.parameter = parameter;
	}
	
	/**
	 * @return the method parameter
	 */
	public Parameter getParameter()
	{
		return parameter;
	}
	
	/**
	 * gets the class type of the parameter attached to this expectation, also
	 * automatically strips out primitives into their object version
	 * 
	 * @return the type of the parameter accessible via {@link #getParameter()}
	 */
	public Class<?> getType()
	{
		return IntelliCommandPrimitiveMapper.outmapPrimitiveClasses(getParameter().getType());
	}
	
	/**
	 * attempts to fill the expectation with a object this method will fail if
	 * the object isn't of the same type as is expected (viewable via
	 * {@link #getType()}) or a parent-type
	 * 
	 * @param object
	 *            the object to fill the expectation with
	 * @throws IllegalArgumentException
	 *             the supplied <tt>object</tt> wasn't of same class or
	 *             parent-class as the {@link Expectation} had
	 */
	public <T> void fill(T object)
	{
		fill(object.getClass(), () -> object);
	}
	
	/**
	 * same as {@link #fill(Object)} but using a supplier instead, this is often
	 * used by bundles as they might operate by {@link Supplier}'s too
	 * 
	 * @param objectClass
	 *            the class that the supplier gives
	 * @param supplier
	 *            the supplier to supply an object to this expectation
	 */
	public void fill(Class<?> objectClass, Supplier<?> supplier)
	{
		if (getType().isAssignableFrom(objectClass) == false)
		{
			throw new IllegalArgumentException("unable to fill expectation, invalid type " + objectClass);
		}
		if (isMet())
		{
			throw new IllegalStateException("expectation already filled");
		}
		valueSupplier = supplier;
	}
	
	/**
	 * @return true if the {@link Expectation} is met
	 */
	public boolean isMet()
	{
		return (valueSupplier != null);
	}
	
	/**
	 * @return the internally stored {@link Expectation}
	 * @throws IllegalArgumentException
	 *             if the {@link Expectation} hasn't been filled
	 */
	public Object extractValue()
	{
		if (isMet() == false)
		{
			throw new IllegalArgumentException("expectation not met, cant supply");
		}
		return valueSupplier.get();
	}
	
	@Override
	public String toString()
	{
		return "Expectation [parameter=" + parameter + ", met=" + isMet() + "]";
	}
	
}
