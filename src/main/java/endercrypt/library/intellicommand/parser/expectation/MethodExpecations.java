/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser.expectation;


import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * container with a list of expectation to be fulfilled each {@link Expectation}
 * represents a {@link Parameter} in a command {@link Method} that needs to be
 * fulfilled in order for a parsing attempt to be successful and the command to
 * be executed
 * 
 * @author EnderCrypt
 */
public class MethodExpecations
{
	private Expectation[] expectations;
	
	public MethodExpecations(Parameter[] parameters)
	{
		expectations = Arrays.stream(parameters).map(Expectation::new).toArray(Expectation[]::new);
	}
	
	/**
	 * @return true if all {@link Expectation}'s have been met
	 */
	public boolean isMet()
	{
		return getUnmet().findAny().isPresent() == false;
	}
	
	/**
	 * @return a stream of all available {@link Expectation}'s
	 */
	public Stream<Expectation> getAll()
	{
		return Arrays.stream(expectations);
	}
	
	/**
	 * @return a stream of all met {@link Expectation}'s
	 */
	public Stream<Expectation> getMet()
	{
		return getAll().filter(Expectation::isMet);
	}
	
	/**
	 * @return a stream of all unmet {@link Expectation}'s
	 */
	public Stream<Expectation> getUnmet()
	{
		return getAll().filter(e -> e.isMet() == false);
	}
	
	/**
	 * this method goes through all {@link Expectation}'s and extracts their
	 * supplied items into an array in preparation for calling the relevant
	 * command method
	 * 
	 * @return an Object[] array with all arguments
	 */
	public Object[] generateArguments()
	{
		if (isMet() == false)
		{
			throw new IllegalStateException("cant generate values as the expectations failed");
		}
		return getAll().map(Expectation::extractValue).toArray();
	}
	
	/**
	 * @return the amount of expectations
	 */
	public int count()
	{
		return expectations.length;
	}
	
	@Override
	public String toString()
	{
		return Arrays.stream(expectations).map(Expectation::toString).collect(Collectors.joining("\n"));
	}
}
