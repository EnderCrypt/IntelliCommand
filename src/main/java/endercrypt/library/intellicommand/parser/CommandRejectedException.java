/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.parser;

/**
 * exception used by a {@link ParserModule} to explicitly signal a command
 * string as impossible to execute for the command being tested
 * 
 * @author EnderCrypt
 */
public class CommandRejectedException extends Exception
{
	private static final long serialVersionUID = 4102041428696914851L;
	
	/**
	 * 
	 */
	
	public CommandRejectedException()
	{
		// empty
	}
	
	public CommandRejectedException(String message)
	{
		super(message);
	}
	
	public CommandRejectedException(Throwable cause)
	{
		super(cause);
	}
	
	public CommandRejectedException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public CommandRejectedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
