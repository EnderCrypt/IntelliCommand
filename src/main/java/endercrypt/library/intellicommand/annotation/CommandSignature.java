/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.annotation;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * vital annotation, marking a class and a method as a command to be used the
 * {@link #value()} from the {@link CommandSignature} on both the Class and the
 * method will be added together into become the actual command that needs to be
 * invoked
 * 
 * <pre>
 * <code>
 *&#64;CommandSignature("system")
 *public class SystemCommands
 *{
 *	&#64;CommandSignature("start {name}")
 *	public void start(@CommandParam("name") String name)
 *	{
 *		System.out.println("Starting system "+name);
 *	}
 *}
 * </code>
 * </pre>
 * 
 * @author EnderCrypt
 */
@Retention(RUNTIME)
@Target({ TYPE, METHOD })
public @interface CommandSignature
{
	public String value();
}
