/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.annotation;


import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * command method annotation is used to request an object from a bundle/ this is
 * done by adding {@code @Include} as an annotation for a command method
 * parameter with the value being the key for the bundle value you want.
 * 
 * <blockquote>
 * 
 * <pre>
 * 
 * &#64;CommandSignature("info version")
 * public void bundle_1(@Include("version") Version version)
 * {
 * 	System.out.println("The current version is " + version);
 * }
 * 
 * </pre>
 * 
 * </blockquote>
 * 
 * @author EnderCrypt
 * 
 * @see endercrypt.library.intellicommand.bundle.Bundle
 */
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface Include
{
	public String value();
}
