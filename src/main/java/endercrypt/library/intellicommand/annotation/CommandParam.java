/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.annotation;


import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * marks a variable to be extracted from the command, and filled into the
 * command method parameter. the {@link #value()} should be in an appropriate
 * place in {@link CommandSignature} surrounded by { and }
 * 
 * @author EnderCrypt
 */
@Retention(RUNTIME)
@Target(PARAMETER)
public @interface CommandParam
{
	public static final String PARAMETER_NAME_AUTOMATIC = "__AUTOMATIC__";
	
	public String value() default PARAMETER_NAME_AUTOMATIC;
}
