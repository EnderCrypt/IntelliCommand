/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.mapper;

/**
 * exception thrown when by a {@link ArgMapper} for when a mapper failed to
 * convert an argument into its target class and that the mapper system should
 * completly abort processing super classes and fail for this command/argument
 * combination.
 * 
 * @author EnderCrypt
 */
public class MapperConversionAbort extends Exception
{
	private static final long serialVersionUID = 8704769971296684296L;
	
	/**
	 * 
	 */
	
	public MapperConversionAbort()
	{
		super();
	}
}
