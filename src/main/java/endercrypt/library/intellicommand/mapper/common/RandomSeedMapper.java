/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.mapper.common;


import java.util.Random;

import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.mapper.ArgumentMapper;
import endercrypt.library.intellicommand.mapper.MapperConversionFail;


/**
 * This {@link ArgMapper} performs conversion from text into a {@link Random} by
 * converting the text into a seed first
 * 
 * @author EnderCrypt
 */
public class RandomSeedMapper implements ArgumentMapper<Random>
{
	@Override
	public Random map(Bundle bundle, Class<? extends Random> targetClass, String text) throws MapperConversionFail
	{
		return new Random(generateLongSeed(text));
	}
	
	/**
	 * utility method for converting a string into a long, either directly or by
	 * hashing it using {@link #longHashString(String)}
	 * 
	 * @param seed
	 *            a number string or any text
	 * @return long seed representation out of seed parameter
	 */
	public static long generateLongSeed(String seed)
	{
		try
		{
			return Long.parseLong(seed);
		}
		catch (NumberFormatException e)
		{
			return longHashString(seed);
		}
	}
	
	/**
	 * utility method for generating a 64 bit (long) hash out of a string
	 * 
	 * source: https://stackoverflow.com/a/1660613
	 * 
	 * @param string
	 *            to hash
	 * @return long hashed number representing the input string
	 */
	public static long longHashString(String string)
	{
		long h = 1125899906842597L; // prime
		int len = string.length();
		
		for (int i = 0; i < len; i++)
		{
			h = 31 * h + string.charAt(i);
		}
		return h;
	}
}
