/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.mapper.common;


import java.net.MalformedURLException;
import java.net.URL;

import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.mapper.ArgumentMapper;
import endercrypt.library.intellicommand.mapper.MapperConversionFail;


/**
 * This {@link ArgMapper} performs conversion from text into a {@link URL}
 * 
 * @author EnderCrypt
 */
public class UrlMapper implements ArgumentMapper<URL>
{
	@Override
	public URL map(Bundle bundle, Class<? extends URL> targetClass, String text) throws MapperConversionFail
	{
		try
		{
			return new URL(text);
		}
		catch (MalformedURLException e)
		{
			throw new MapperConversionFail();
		}
	}
}
