/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.mapper;


import endercrypt.library.intellicommand.bundle.Bundle;

import java.util.ArrayList;


/**
 * {@link ArrayList} subclass designed for holding {@link ArgumentMapper}'s.
 * used internally by {@link Mappers}.
 * 
 * @author EnderCrypt
 */
@SuppressWarnings("serial")
public class ClassMapperCollection<T> extends ArrayList<ArgumentMapper<? extends T>> implements ArgumentMapper<T>
{
	@SuppressWarnings("unchecked")
	@Override
	public T map(Bundle bundle, Class<? extends T> targetClass, String text) throws MapperConversionFail
	{
		for (ArgumentMapper<? extends T> mapper : this)
		{
			try
			{
				T value = ((ArgumentMapper<T>) mapper).map(bundle, targetClass, text);
				if (value == null)
				{
					throw new NullPointerException(mapper.getClass() + " returned null");
				}
				return value;
			}
			catch (MapperConversionFail e)
			{
				continue;
			}
			catch (MapperConversionAbort e)
			{
				break;
			}
		}
		
		throw new MapperConversionFail();
	}
}
