/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.mapper;


import endercrypt.library.intellicommand.bundle.Bundle;


/**
 * interface for creating your own ArgumentMapper the map method should attempt
 * to convert to the correct java class (the class type passed into
 * registerMapper)
 * 
 * if the class is unable to convert due to invalid text, a MapperConversionFail
 * should be thrown
 * 
 * @author EnderCrypt
 */
public interface ArgumentMapper<T>
{
	/**
	 * mapping method for converting a text string into a java object
	 * 
	 * @param bundle
	 *            the bundle fed alongside this command
	 * @param targetClass
	 *            the class that was requested
	 * @param text
	 *            text be converted
	 * @return the result of mapping
	 * @throws MapperConversionFail
	 *             if string can't be converted
	 */
	public T map(Bundle bundle, Class<? extends T> targetClass, String text) throws MapperConversionFail, MapperConversionAbort;
}
