/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.mapper;


import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.exception.IntelliCommandException;
import endercrypt.library.intellicommand.mapper.common.BigDecimalMapper;
import endercrypt.library.intellicommand.mapper.common.BigIntegerMapper;
import endercrypt.library.intellicommand.mapper.common.EnumMapper;
import endercrypt.library.intellicommand.mapper.common.FileMapper;
import endercrypt.library.intellicommand.mapper.common.InetAddressMapper;
import endercrypt.library.intellicommand.mapper.common.PathMapper;
import endercrypt.library.intellicommand.mapper.common.RandomSeedMapper;
import endercrypt.library.intellicommand.mapper.common.RegexPatternMapper;
import endercrypt.library.intellicommand.mapper.common.StringMapper;
import endercrypt.library.intellicommand.mapper.common.UriMapper;
import endercrypt.library.intellicommand.mapper.common.UrlMapper;
import endercrypt.library.intellicommand.mapper.common.UuidMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.BooleanMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.ByteMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.CharMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.DoubleMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.FloatMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.IntegerMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.LongMapper;
import endercrypt.library.intellicommand.mapper.common.primitive.ShortMapper;
import endercrypt.library.intellicommand.utility.IntelliCommandPrimitiveMapper;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;


/**
 * @author EnderCrypt
 * 
 *         class that holds all mappers, many {@link ArgumentMapper}'s will be
 *         added automatically including but not limited to all primitives,
 *         {@link String}, {@link File} and {@link Path}
 */
public class Mappers
{
	private Map<Class<?>, ClassMapperCollection<?>> mappers = new HashMap<>();
	
	/**
	 * install the primitive and basic mappers
	 */
	public Mappers()
	{
		// primitive
		register(boolean.class, new BooleanMapper());
		register(byte.class, new ByteMapper());
		register(char.class, new CharMapper());
		register(double.class, new DoubleMapper());
		register(float.class, new FloatMapper());
		register(int.class, new IntegerMapper());
		register(long.class, new LongMapper());
		register(short.class, new ShortMapper());
		
		// common
		register(Enum.class, new EnumMapper());
		register(String.class, new StringMapper());
		register(File.class, new FileMapper());
		register(Path.class, new PathMapper());
		register(URL.class, new UrlMapper());
		register(URI.class, new UriMapper());
		register(UUID.class, new UuidMapper());
		register(InetAddress.class, new InetAddressMapper());
		register(Pattern.class, new RegexPatternMapper());
		register(Random.class, new RandomSeedMapper());
		register(BigDecimal.class, new BigDecimalMapper());
		register(BigInteger.class, new BigIntegerMapper());
	}
	
	@SuppressWarnings("unchecked")
	public <T> void register(Class<T> targetClass, ArgumentMapper<? extends T> mapper)
	{
		if (targetClass.isPrimitive())
		{
			targetClass = (Class<T>) IntelliCommandPrimitiveMapper.getPrimitiveBoxedClass(targetClass);
		}
		ClassMapperCollection<T> classMappers = getClassMapper(targetClass);
		if (classMappers.contains(mapper))
		{
			throw new IntelliCommandException("A mapper already exists for " + targetClass.getName());
		}
		classMappers.add(mapper);
	}
	
	/**
	 * returns a {@link ClassMapperCollection} designed for holding relevant
	 * {@link ArgumentMapper}. any modification to the resulting
	 * {@link ClassMapperCollection} is propagated to the underlying list.
	 * 
	 * @param targetClass
	 *            the class whose {@link ClassMapperCollection} should be
	 *            returned
	 * @return a {@link ClassMapperCollection}
	 * @see #getCompleteClassMapper(Class)
	 */
	@SuppressWarnings("unchecked")
	private <T> ClassMapperCollection<T> getClassMapper(Class<T> targetClass)
	{
		return (ClassMapperCollection<T>) mappers.computeIfAbsent(targetClass, (clazz) -> new ClassMapperCollection<>());
	}
	
	/**
	 * works similar to {@link #getClassMapper(Class)} but instead also includes
	 * all ClassMappers for all sub-classes of targetClass. modifications to the
	 * resulting {@link ClassMapperCollection} are not propogated to the
	 * underlying class mapper list.
	 * 
	 * @param targetClass
	 * @return
	 * @see #getClassMapper(Class)
	 */
	@SuppressWarnings("unchecked")
	private <T> ClassMapperCollection<T> getCompleteClassMapper(Class<T> targetClass)
	{
		ClassMapperCollection<T> collection = new ClassMapperCollection<>();
		
		Class<?> localClass = targetClass;
		do
		{
			for (Entry<Class<?>, ClassMapperCollection<?>> entry : mappers.entrySet())
			{
				Class<?> checkClass = entry.getKey();
				ClassMapperCollection<?> targetClassMappers = entry.getValue();
				
				if (localClass.isAssignableFrom(checkClass))
				{
					collection.addAll((ClassMapperCollection<T>) targetClassMappers);
				}
			}
			localClass = localClass.getSuperclass();
		}
		while (localClass != null);
		
		return collection;
	}
	
	/**
	 * attempts to perform a Argument conversion from a String into an object of
	 * type <code>targetClass</code>. the resulting object map be of type
	 * <code>targetClass</code> or one of its sub-classes. if conversion fails,
	 * a MapperConversionFail will be thrown
	 * 
	 * @param bundle
	 *            to pass along to the mappers
	 * @param targetClass
	 *            to convert the text into
	 * @param text
	 *            to convert
	 * @return an instance of class type <code>targetClass</code>
	 * @throws MapperConversionFail
	 *             if the conversion failed
	 */
	public <T> T map(Bundle bundle, Class<T> targetClass, String text) throws MapperConversionFail
	{
		ClassMapperCollection<T> mappers = getCompleteClassMapper(targetClass);
		if (mappers.isEmpty())
		{
			throw new IntelliCommandException("Couldnt find mapper for class " + targetClass.getSimpleName());
		}
		return mappers.map(bundle, targetClass, text);
	}
	
	@Override
	public String toString()
	{
		return mappers.keySet().toString();
	}
}
