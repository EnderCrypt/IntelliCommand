/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.bundle;


import endercrypt.library.intellicommand.utility.CacheSupplier;

import java.util.Objects;
import java.util.function.Supplier;


/**
 * {@link BundleEntry} represents a key and an object which will be
 * automatically fed to command parameters that have the {@link Include}
 * annotation with its {@link String} value set to the key of an entry in the
 * {@link Bundle}.
 * 
 * a {@link BundleEntry} can be created and immediately attached to a
 * {@link Bundle} by calling {@link Bundle#attach()}.
 * 
 * @author EnderCrypt
 */
public class BundleEntry
{
	private final String key;
	private final Class<?> bundleClass;
	private final Supplier<Object> supplier;
	
	protected BundleEntry(String key, Class<?> bundleClass, Supplier<Object> supplier)
	{
		super();
		this.key = Objects.requireNonNull(key, "key");
		this.bundleClass = Objects.requireNonNull(bundleClass, "bundleClass");
		this.supplier = CacheSupplier.wrap(supplier);
	}
	
	/**
	 * @return the key of this {@link BundleEntry}
	 */
	public String getKey()
	{
		return key;
	}
	
	/**
	 * @return the class of the value stored inside this bundle entry
	 */
	public Class<?> getBundleClass()
	{
		return bundleClass;
	}
	
	/**
	 * - * @return the supplier that contains the bundle value -
	 */
	public Supplier<Object> getSupplier()
	{
		return supplier;
	}
	
	/**
	 * @return the value stored inside this {@link BundleEntry}
	 */
	public Object supply()
	{
		return supplier.get();
	}
	
	@Override
	public String toString()
	{
		return getKey() + " -> " + getBundleClass() + " (" + supply() + ")";
	}
}
