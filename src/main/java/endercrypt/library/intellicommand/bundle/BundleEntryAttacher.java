/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.bundle;


import java.util.Objects;
import java.util.function.Supplier;


/**
 * assistant class for {@link Bundle} for assisting in adding bundle entries
 * quickly, this class can be accessed by simply calling {@link Bundle#attach()}
 * 
 * @author EnderCrypt
 */
public class BundleEntryAttacher
{
	private final Bundle bundle;
	
	protected BundleEntryAttacher(Bundle bundle)
	{
		this.bundle = Objects.requireNonNull(bundle, "bundle");
	}
	
	/**
	 * returns the bundle associated with this BundleEntryAttacher
	 * 
	 * @return the Bundle
	 */
	public Bundle getBundle()
	{
		return bundle;
	}
	
	/**
	 * Creates a simple {@link BundleEntry} which holds a single item and adds
	 * its it to the Bundle
	 * 
	 * @param key
	 *            to generate this entry with
	 * @param object
	 *            to generate this entry with
	 * @return itself
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> BundleEntryAttacher instance(String key, T object)
	{
		getter(key, (Class<T>) object.getClass(), () -> object);
		return this;
	}
	
	/**
	 * Creates a {@link BundleEntry} which does not immidietly hold the item
	 * intended for command, but instead a getter which will be called only if a
	 * command with relevant argument {@code @Include}'s requests one
	 * 
	 * keep in mind that even if the command doesent use an instance, it may
	 * still be requested by the BundleEntry
	 * 
	 * @param key
	 *            to generate this entry with
	 * @param bundleClass
	 *            the class of the object that will be returned by the supplier
	 * @param supplier
	 *            that will supply the object
	 * @return itself
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> BundleEntryAttacher getter(String key, Class<T> bundleClass, Supplier<T> supplier)
	{
		bundle.add(new BundleEntry(key, bundleClass, (Supplier<Object>) supplier));
		return this;
	}
}
