/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.bundle;


import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @author EnderCrypt
 *
 *         bundle for holding objects to be @Include in command methods
 */
public class Bundle
{
	/**
	 * Convenience technique for creating a new Bundle, and then obtaining then
	 * associated BundleEntryAttacher. this can then be used to chain
	 * BundleEntry's
	 * 
	 * <blockquote>
	 * 
	 * <pre>
	 * Bundle bundle = Bundle.createWith()
	 * 	.instance("data", data)
	 * 	.getBundle();
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * @return the BundleEntryAttacher
	 */
	public static BundleEntryAttacher createWith()
	{
		Bundle bundle = new Bundle();
		return bundle.attach();
	}
	
	private final Map<String, BundleEntry> bundles = new HashMap<>();
	private final BundleEntryAttacher adder = new BundleEntryAttacher(this);
	
	/**
	 * 
	 * @return a {@link BundleEntryAdder} for easy creation and adding of
	 *         {@link BundleEntry}'s.
	 */
	public BundleEntryAttacher attach()
	{
		return adder;
	}
	
	/**
	 * directly adds a {@link BundleEntry} to this Bundle.
	 * 
	 * @param entry
	 *            to add
	 */
	protected void add(BundleEntry entry)
	{
		Objects.requireNonNull(entry, "entry");
		bundles.put(entry.getKey().toLowerCase(), entry);
	}
	
	/**
	 * checks if a {@link BundleEntry} is available for the specified key
	 * 
	 * @param key
	 *            the key to check for a bundle at
	 * @return true if the bundle exists
	 */
	public boolean has(String key)
	{
		return get(key) != null;
	}
	
	/**
	 * returns the {@link BundleEntry} available at a certain key if no bundle
	 * is accessible at that key, null will be returned. you can use
	 * {@link #has(String)} to check if an entry exists for the key
	 * 
	 * @param key
	 *            the key to get a bundle from
	 * @return the {@link BundleEntry} held by this key or null
	 */
	public BundleEntry get(String key)
	{
		key = key.toLowerCase();
		return bundles.get(key);
	}
	
	@Override
	public String toString()
	{
		return bundles.values().stream().map(BundleEntry::toString).collect(Collectors.joining("\n"));
	}
}
