/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.signature.pieces;


import endercrypt.library.intellicommand.signature.VocalPiece;

import java.util.Objects;


/**
 * a piece of a signature, that is just a normal text
 * 
 * @author EnderCrypt
 */
public class PlainPiece extends VocalPiece
{
	private String text;
	
	public PlainPiece(String text)
	{
		this.text = Objects.requireNonNull(text, "text");
	}
	
	/**
	 * @return the text of the signature
	 */
	public String getText()
	{
		return text;
	}
	
	@Override
	public String toLiteral()
	{
		return getText();
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof PlainPiece)) return false;
		PlainPiece other = (PlainPiece) obj;
		if (text == null)
		{
			if (other.text != null) return false;
		}
		else if (!text.equals(other.text)) return false;
		return true;
	}
}
