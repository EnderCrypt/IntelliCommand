
/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.signature.pieces;


import endercrypt.library.intellicommand.annotation.CommandParam;
import endercrypt.library.intellicommand.signature.SignatureArray;
import endercrypt.library.intellicommand.signature.VocalPiece;

import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * a piece of the command method signature that that represents a variable,
 * usually surrounded by { and } the variable value will be accessible using
 * {@link CommandParam}
 * 
 * @author EnderCrypt
 */
public class VariablePiece extends VocalPiece
{
	public static String toNames(List<VariablePiece> variables)
	{
		return variables.stream()
			.map(VariablePiece::getName)
			.collect(Collectors.toList())
			.toString();
	}
	
	public static List<VariablePiece> resolve(Parameter[] parameters)
	{
		return Arrays.stream(parameters)
			.map(VariablePiece::resolveParameterName)
			.filter(Objects::nonNull)
			.map(VariablePiece::new)
			.collect(Collectors.toList());
	}
	
	public static List<VariablePiece> resolve(SignatureArray signatures)
	{
		return Arrays.stream(signatures.getPieces())
			.filter(VariablePiece.class::isInstance)
			.map(VariablePiece.class::cast)
			.collect(Collectors.toList());
	}
	
	private static String resolveParameterName(Parameter parameter)
	{
		CommandParam annotation = parameter.getAnnotation(CommandParam.class);
		if (annotation == null)
		{
			return null;
		}
		
		String literalName = annotation.value();
		
		if (literalName.equals(CommandParam.PARAMETER_NAME_AUTOMATIC))
		{
			return parameter.getName();
		}
		
		return literalName;
	}
	
	private String name;
	
	public VariablePiece(String name)
	{
		this.name = Objects.requireNonNull(name, "name");
	}
	
	/**
	 * @return the variable name
	 */
	public String getName()
	{
		return name;
	}
	
	@Override
	public String toLiteral()
	{
		return "{" + getName() + "}";
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.toLowerCase().hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof VariablePiece)) return false;
		VariablePiece other = (VariablePiece) obj;
		if (name == null)
		{
			if (other.name != null) return false;
		}
		else if (!name.equalsIgnoreCase(other.name)) return false;
		return true;
	}
}
