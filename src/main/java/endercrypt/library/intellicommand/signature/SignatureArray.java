/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.signature;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import endercrypt.library.intellicommand.signature.pieces.PlainPiece;
import endercrypt.library.intellicommand.signature.pieces.VariablePiece;


/**
 * represents a signature, which is generated when registering a command with
 * methods that have {@link CommandSignature}, whose values will be converted
 * into a {@link SignatureArray}
 * 
 * @author EnderCrypt
 */
/**
 * @author EnderCrypt
 *
 */
public class SignatureArray
{
	/**
	 * goes trough a signature text (one word) to generate a piece
	 * 
	 * @param word
	 *            to check
	 * @return piece to represent this signature word
	 */
	private static Piece generatePiece(String text)
	{
		if (text.startsWith("{") && text.endsWith("}"))
		{
			String name = text.substring(1, text.length() - 1);
			return new VariablePiece(name);
		}
		
		// default
		return new PlainPiece(text);
	}
	
	private boolean primary;
	private Piece[] pieces;
	
	public SignatureArray(boolean primary, String[] signature)
	{
		this.primary = primary;
		pieces = new Piece[signature.length];
		
		for (int i = 0; i < signature.length; i++)
		{
			String text = signature[i];
			pieces[i] = generatePiece(text);
		}
	}
	
	/**
	 * @return weather or not this alias is a primary alias
	 */
	public boolean isPrimary()
	{
		return primary;
	}
	
	/**
	 * the amount of signature {@link Piece}'s
	 * 
	 * @return the length
	 */
	public int length()
	{
		return pieces.length;
	}
	
	/**
	 * returns a signature {@link Piece} from an index
	 * 
	 * @param index
	 *            of the {@link Piece} to retrieve
	 * @return a {@link Piece} that can be casted to use for parsing
	 */
	public Piece get(int index)
	{
		return pieces[index];
	}
	
	/**
	 * returns an array of all internal pieces
	 * 
	 * @return an array of {@link Piece}'s
	 */
	public Piece[] getPieces()
	{
		return Arrays.copyOf(pieces, pieces.length);
	}
	
	/**
	 * returns the Plain prefix of the signature, basically the first part of
	 * the command that is unique and not bearing any command arguments for
	 * example, in the following command:
	 * <code>@CommandSignature("shop browse {search-term}")</code> this method
	 * would return "shop browse"
	 * 
	 * @return the identifying prefix of the signature
	 */
	public String getIdentifyingPrefix()
	{
		StringBuilder sb = new StringBuilder();
		for (Piece piece : pieces)
		{
			if ((piece instanceof PlainPiece) == false)
			{
				break;
			}
			sb.append(piece.toLiteral()).append(" ");
		}
		if (sb.length() > 0)
		{
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}
	
	/**
	 * returns all the pieces in this {@link SignatureArray} that are
	 * {@link VariablePiece}'s
	 * 
	 * @return a list of {@link VariablePiece}'s
	 */
	public List<VariablePiece> getVariables()
	{
		return Arrays.stream(pieces)
			.filter(VariablePiece.class::isInstance)
			.map(VariablePiece.class::cast)
			.collect(Collectors.toList());
	}
	
	/**
	 * @return the whole signature as a complete string
	 */
	public String asText()
	{
		return Arrays.stream(pieces)
			.map(Piece::toLiteral)
			.collect(Collectors.joining(" "));
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(pieces);
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof SignatureArray)) return false;
		SignatureArray other = (SignatureArray) obj;
		if (!Arrays.equals(pieces, other.pieces)) return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		String aliasText = isPrimary() ? "" : "(alias) ";
		return "Signature [" + aliasText + "\"" + asText() + "\"]";
	}
}
