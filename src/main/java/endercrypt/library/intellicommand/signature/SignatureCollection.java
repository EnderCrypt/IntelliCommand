/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.signature;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import endercrypt.library.intellicommand.annotation.CommandAlias;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.exception.IntelliCommandBuildException;
import endercrypt.library.intellicommand.exception.IntelliCommandException;


/**
 * holds all signatures bound to a command method, this guarantees a primary
 * {@link SignatureArray} and potential aliases using {@link #getAliases()}
 * 
 * @author EnderCrypt
 */
public class SignatureCollection implements Iterable<SignatureArray>
{
	/**
	 * constructs a {@link SignatureArray} based on a class and method signature
	 * string
	 * 
	 * @param primary
	 *            weather or not this {@link SignatureArray} should be marked as
	 *            primary
	 * @param classSignature
	 * @param methodSignature
	 * @return {@link SignatureArray} representation
	 */
	private static SignatureArray generate(boolean primary, String classSignature, String methodSignature)
	{
		String signature = classSignature + " " + methodSignature;
		String[] args = Arrays.stream(signature.split(" ")).filter(s -> !s.equals("")).toArray(String[]::new);
		return new SignatureArray(primary, args);
	}
	
	private SignatureArray primary;
	private List<SignatureArray> aliases;
	
	public SignatureCollection(Class<?> methodClass, Method method)
	{
		// global signature
		CommandSignature classSignatureAnnotation = methodClass.getAnnotation(CommandSignature.class);
		if (classSignatureAnnotation == null)
		{
			throw new IntelliCommandBuildException(methodClass, method, "missing class annotation " + CommandSignature.class.getSimpleName());
		}
		String classSignature = classSignatureAnnotation.value();
		
		// primary
		CommandSignature localCommandSignature = method.getAnnotation(CommandSignature.class);
		if (localCommandSignature == null)
		{
			throw new IntelliCommandException(method + " lacks " + CommandSignature.class.getSimpleName());
		}
		primary = generate(true, classSignature, localCommandSignature.value());
		
		// aliases
		Set<SignatureArray> aliasesSet = new HashSet<>();
		if (methodClass.isAnnotationPresent(CommandAlias.class) || method.isAnnotationPresent(CommandAlias.class))
		{
			Set<String> classAliases = new HashSet<>();
			classAliases.add(classSignature);
			classAliases.addAll(Optional
				.ofNullable(methodClass.getAnnotation(CommandAlias.class))
				.map(CommandAlias::value)
				.stream()
				.flatMap(Arrays::stream)
				.map(String::trim)
				.collect(Collectors.toList()));
			
			Set<String> methodAliases = new HashSet<>();
			methodAliases.add(localCommandSignature.value());
			methodAliases.addAll(Optional
				.ofNullable(method.getAnnotation(CommandAlias.class))
				.map(CommandAlias::value)
				.stream()
				.flatMap(Arrays::stream)
				.map(String::trim)
				.collect(Collectors.toList()));
			
			for (String classAlias : classAliases)
			{
				for (String methodAlias : methodAliases)
				{
					aliasesSet.add(generate(false, classAlias, methodAlias));
				}
			}
			aliasesSet.remove(primary);
		}
		
		aliases = new ArrayList<>(aliasesSet);
	}
	
	/**
	 * @return the primary {@link SignatureArray}
	 */
	public SignatureArray getPrimary()
	{
		return primary;
	}
	
	/**
	 * @return weather or not there are {@link SignatureArray}'s available using
	 *         {@link #getAliases()}
	 */
	public boolean hasAliases()
	{
		return aliases.size() > 0;
	}
	
	/**
	 * @return a potential zero-size list of all {@link SignatureArray}'s
	 */
	public List<SignatureArray> getAliases()
	{
		return Collections.unmodifiableList(aliases);
	}
	
	/**
	 * iterator that loops through all signatures (primary & alias)
	 */
	@Override
	public Iterator<SignatureArray> iterator()
	{
		List<SignatureArray> signatures = new ArrayList<>();
		signatures.add(getPrimary());
		signatures.addAll(getAliases());
		return signatures.iterator();
	}
}
