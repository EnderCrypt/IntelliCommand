/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.exception.execution;


import java.util.Collections;
import java.util.List;

import endercrypt.library.intellicommand.command.Command;


/**
 * exception for when the command exists, but arguments didn't match
 * 
 * @author EnderCrypt
 */
public class CommandArgumentMismatchException extends CommandExecutionException
{
	private static final long serialVersionUID = -1966195340859059782L;
	
	/**
	 * 
	 */
	
	private final String commandString;
	private final List<Command> commands;
	
	public CommandArgumentMismatchException(String commandString, List<Command> commands)
	{
		super();
		this.commandString = commandString;
		this.commands = Collections.unmodifiableList(commands);
	}
	
	/**
	 * @return the command that was executed
	 */
	public String getCommandString()
	{
		return commandString;
	}
	
	/**
	 * @return the commands whose text matched, but arguments didn't
	 */
	public List<Command> getCommands()
	{
		return commands;
	}
}
