/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.exception.execution;


import endercrypt.library.intellicommand.command.Command;


/**
 * exception indicating the a command method was found and executed, but threw
 * an exception this exception can be accessed using {@link #getCause()}
 * 
 * @author EnderCrypt
 */
public class UnderlyingCommandException extends CommandExecutionException
{
	private static final long serialVersionUID = -3060256054134608782L;
	
	/**
	 * 
	 */
	
	private final Command command;
	private final Object[] arguments;
	
	public UnderlyingCommandException(Command command, Object[] arguments, Throwable cause)
	{
		super(cause);
		this.command = command;
		this.arguments = arguments;
	}
	
	/**
	 * @return the command that threw the exception
	 */
	public Command getCommand()
	{
		return command;
	}
	
	/**
	 * @return the arguments supplied into the command method
	 */
	public Object[] getArguments()
	{
		return arguments;
	}
}
