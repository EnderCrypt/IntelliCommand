/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.exception.execution;

/**
 * the command text that was executed did not have any matching commands
 * 
 * @author EnderCrypt
 */
public class CommandNotFoundException extends CommandExecutionException
{
	private static final long serialVersionUID = -1966195340859059782L;
	
	/**
	 * 
	 */
	
	private final String commandString;
	
	public CommandNotFoundException(String commandString)
	{
		super();
		this.commandString = commandString;
	}
	
	/**
	 * @return the command that was executed
	 */
	public String getCommandString()
	{
		return commandString;
	}
}
