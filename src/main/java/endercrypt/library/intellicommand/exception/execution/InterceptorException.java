/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.exception.execution;


import endercrypt.library.intellicommand.command.Command;


/**
 * thrown when an
 * {@link endercrypt.library.intellicommand.interceptor.Interceptor} throws an
 * exception {@link #getCause()} will reveal the full exception that caused
 * this. this class is a subclass of {@link UnderlyingCommandException} and as
 * such will behave similarly.
 * 
 * @author EnderCrypt
 */
public class InterceptorException extends UnderlyingCommandException
{
	private static final long serialVersionUID = -3562410411679584890L;
	
	/**
	 * 
	 */
	
	public InterceptorException(Command command, Object[] arguments, Throwable cause)
	{
		super(command, arguments, cause);
	}
}
