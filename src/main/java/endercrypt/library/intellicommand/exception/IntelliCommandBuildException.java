/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.exception;


import java.lang.reflect.Method;


/**
 * Exception indicating that IntelliCommand failed to properly register/setup a
 * class for being a command class
 * 
 * @author EnderCrypt
 */
public class IntelliCommandBuildException extends IntelliCommandException
{
	private static final long serialVersionUID = -1753648346005849924L;
	
	/**
	 * 
	 */
	
	private final Class<?> methodClass;
	private final Method method;
	
	public IntelliCommandBuildException(Class<?> methodClass, Method method, String message)
	{
		super(message);
		this.methodClass = methodClass;
		this.method = method;
	}
	
	public Class<?> getMethodClass()
	{
		return methodClass;
	}
	
	public Method getMethod()
	{
		return method;
	}
}
