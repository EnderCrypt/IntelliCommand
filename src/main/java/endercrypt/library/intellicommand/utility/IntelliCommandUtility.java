/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.utility;

/**
 * general utility class
 *
 * @author EnderCrypt
 */
public class IntelliCommandUtility
{
	/**
	 * utility class, use static methods instead
	 */
	private IntelliCommandUtility()
	{
		throw new UnsupportedOperationException();
	}
	
	/**
	 * capitalizes a text, text is unmodified if null or too short
	 * 
	 * @param description
	 * @return capitalized version of the input
	 */
	public static String capitalize(String description)
	{
		if (description == null || description.contentEquals(""))
		{
			return description;
		}
		return Character.toUpperCase(description.charAt(0)) + description.substring(1);
	}
}
