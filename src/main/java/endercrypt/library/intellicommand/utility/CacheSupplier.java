/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.utility;


import java.util.Objects;
import java.util.function.Supplier;


/**
 * wrapping supplier allowing for straightforward caching of the first item
 * supplied by the underlying supplier
 * 
 * @author EnderCrypt
 */
public class CacheSupplier<T> implements Supplier<T>
{
	/**
	 * wraps the provided supplier into a {@link CacheSupplier}
	 * 
	 * @param <T>
	 * @param supplier
	 * @return
	 */
	public static <T> CacheSupplier<T> wrap(Supplier<T> supplier)
	{
		return new CacheSupplier<>(supplier);
	}
	
	private final Supplier<T> supplier;
	private T item = null;
	
	private CacheSupplier(Supplier<T> supplier)
	{
		this.supplier = Objects.requireNonNull(supplier, "supplier");
	}
	
	@Override
	public T get()
	{
		if (item == null)
		{
			item = supplier.get();
		}
		return item;
	}
}
