/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.utility.stringsplit;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;


/**
 * utility class for splitting a string on every space. this class has an
 * advantage over {@code String.split(" ")} in that it also handles quotes
 * properly. StringSplitBuilder should be called using the static method
 * {@link StringSplitBuilder#perform(String)}
 * 
 * @author EnderCrypt
 */
public class StringSplitBuilder
{
	/**
	 * performs a split of string text, on every space, ignoring quoted spaces
	 * 
	 * @param text
	 * @return List of each text segment in the text
	 * @throws MalformedCommandException
	 *             if the text had an un-even amount of quotes
	 */
	public static List<String> perform(String text) throws MalformedCommandException
	{
		StringSplitBuilder builder = new StringSplitBuilder();
		builder.feed(text);
		return builder.finish();
	}
	
	private List<String> segments = new ArrayList<>();
	private StringBuilder buffer = new StringBuilder();
	private boolean inQuote = false;
	
	/**
	 * @return Whether the buffer of text is currently empty
	 */
	private boolean isEmptyBuffer()
	{
		return buffer.length() == 0;
	}
	
	/**
	 * moves the current buffer of text over to the list of segments
	 */
	private void buildSegment()
	{
		if (isEmptyBuffer())
		{
			throw new StringSplitException("no buffer available to add");
		}
		if (inQuote)
		{
			throw new StringSplitException("cannot add buffer while in quote");
		}
		segments.add(buffer.toString());
		buffer.setLength(0);
	}
	
	/**
	 * feeds further texts to StringSplitBuilder for processing
	 * 
	 * @param text
	 *            to feed
	 */
	public void feed(String text)
	{
		for (char c : text.toCharArray())
		{
			feed(c);
		}
	}
	
	/**
	 * feeds further characters to StringSplitBuilder for processing
	 * 
	 * @param c
	 *            to feed
	 */
	public void feed(char c)
	{
		if (c == ' ' && inQuote == false)
		{
			if (isEmptyBuffer() == false)
			{
				buildSegment();
			}
			return;
		}
		if (c == '\"')
		{
			if (inQuote)
			{
				inQuote = false;
				buildSegment();
			}
			else
			{
				inQuote = true;
			}
			return;
		}
		buffer.append(c);
	}
	
	/**
	 * finishes up the StringSplitBuilder process and returns the List of text
	 * segments
	 * 
	 * @return List of each text segment
	 * @throws MalformedCommandException
	 *             if the text had an un-even amount of quotes
	 */
	public List<String> finish() throws MalformedCommandException
	{
		if (inQuote)
		{
			throw new MalformedCommandException("Malformed command, should have starting and ending quotes");
		}
		if (isEmptyBuffer() == false)
		{
			buildSegment();
		}
		return Collections.unmodifiableList(segments);
	}
}
