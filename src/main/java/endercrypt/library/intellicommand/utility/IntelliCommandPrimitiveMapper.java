/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.utility;


import java.util.HashMap;
import java.util.Map;


/**
 * utility for mapping primitive classes into their boxed version
 * 
 * @author EnderCrypt
 */
public class IntelliCommandPrimitiveMapper
{
	/**
	 * utility class, use static methods instead
	 */
	private IntelliCommandPrimitiveMapper()
	{
		throw new UnsupportedOperationException();
	}
	
	private static final Map<Class<?>, Class<?>> primitiveClassMap = new HashMap<>();
	
	static
	{
		primitiveClassMap.put(boolean.class, Boolean.class);
		primitiveClassMap.put(byte.class, Byte.class);
		primitiveClassMap.put(short.class, Short.class);
		primitiveClassMap.put(char.class, Character.class);
		primitiveClassMap.put(int.class, Integer.class);
		primitiveClassMap.put(long.class, Long.class);
		primitiveClassMap.put(float.class, Float.class);
		primitiveClassMap.put(double.class, Double.class);
	}
	
	/**
	 * converts a primitive class (such as int.class) into a boxed class (such
	 * as Integer.class) will throw an exception if the <tt>targetClass</tt>
	 * argument wasnt a primitive class
	 *
	 * @param targetClass
	 *            a primitve class
	 * @return a boxed version of the primitive class
	 * @throws IllegalArgumentException
	 *             if the <tt>targetClass</tt> wasnt a primitive
	 */
	public static Class<?> getPrimitiveBoxedClass(Class<?> targetClass)
	{
		if (primitiveClassMap.containsKey(targetClass) == false)
		{
			throw new IllegalArgumentException(targetClass.getSimpleName() + " is not a primitive");
		}
		return primitiveClassMap.get(targetClass);
	}
	
	/**
	 * practically the same as {@link #getPrimitiveBoxedClass(Class)} except it
	 * does not throw any exception if the <tt>targetClass</tt> isn't primitive,
	 * but will instead return the <tt>targetClass</tt> directly if thats the
	 * case
	 *
	 * @param targetClass
	 *            a not/primitive class
	 * @return a boxed version of the primitive class, or the the input argument
	 *         itself if it wasn't a primitive
	 */
	public static Class<?> outmapPrimitiveClasses(Class<?> targetClass)
	{
		if (targetClass.isPrimitive())
		{
			targetClass = getPrimitiveBoxedClass(targetClass);
		}
		return targetClass;
	}
}
