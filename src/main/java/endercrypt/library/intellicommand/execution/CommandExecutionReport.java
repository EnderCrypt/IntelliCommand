/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.execution;


import java.util.Arrays;
import java.util.Objects;

import endercrypt.library.intellicommand.command.Command;


/**
 * command reports are generated whenever a command is successfully executed
 * using
 * {@link endercrypt.library.intellicommand.IntelliCommand#execute(String, Bundle)}.
 * this class holds information about the execution, such as what command was
 * executed, what arguments it got, what it returned and more.
 * 
 * @author EnderCrypt
 */
public class CommandExecutionReport
{
	private Command command;
	private Object[] arguments;
	private int time;
	private Object returnValue;
	
	public CommandExecutionReport(Command command, Object[] arguments, int time, Object returnValue)
	{
		this.command = Objects.requireNonNull(command, "command");
		this.arguments = Objects.requireNonNull(arguments, "arguments");
		this.time = time;
		this.returnValue = returnValue;
	}
	
	/**
	 * @return the {@link Command} that was executed
	 */
	public Command getCommand()
	{
		return command;
	}
	
	/**
	 * @return the arguments that the command method was executed with
	 */
	public Object[] getArguments()
	{
		return Arrays.copyOf(arguments, arguments.length);
	}
	
	/**
	 * @return the amount of milliseconds that the command method ran for
	 */
	public int getTime()
	{
		return time;
	}
	
	/**
	 * returns the return value produced by calling the command method, will be
	 * null if the command method returned null or its a void method
	 * 
	 * @return the return value
	 */
	public Object getReturnValue()
	{
		return returnValue;
	}
}
