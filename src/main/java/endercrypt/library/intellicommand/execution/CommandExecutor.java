/************************************************************************
z * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package endercrypt.library.intellicommand.execution;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;
import endercrypt.library.intellicommand.parser.CommandParser;
import endercrypt.library.intellicommand.parser.CommandRejectedException;
import endercrypt.library.intellicommand.parser.expectation.MethodExpecations;
import endercrypt.library.intellicommand.utility.stringsplit.StringSplitBuilder;


/**
 * Handles matching and execution of commands for IntelliCommand
 * 
 * @author EnderCrypt
 */
public class CommandExecutor
{
	/**
	 * convenience method for executing a CommandExecutor
	 * 
	 * @param intelliCommand
	 *            instance to attempt execute commands whitin
	 * @param commandString
	 *            to match into a command to execute
	 * @param bundle
	 *            to pass along to the command
	 * @return a {@link CommandExecutionReport} providing information about the
	 *         command execution
	 * @throws UnderlyingCommandException
	 *             if the executed command threw an exception
	 * @throws CommandNotFoundException
	 *             if no matching commands were found
	 * @throws CommandArgumentMismatchException
	 *             if 1 or more matching commands were found, but arguments did
	 *             not match
	 * @throws MalformedCommandException
	 *             if the arguments were broken, such as by a non-closed quote
	 */
	public static CommandExecutionReport perform(IntelliCommand intelliCommand, String commandString, Bundle bundle) throws UnderlyingCommandException, CommandNotFoundException, CommandArgumentMismatchException, MalformedCommandException
	{
		CommandExecutor commandExecutor = new CommandExecutor(intelliCommand);
		return commandExecutor.execute(commandString, bundle);
	}
	
	private final IntelliCommand intelliCommand;
	private final CommandParser commandParser;
	
	private final List<Command> mismatchingArgumentCommands = new ArrayList<>();
	
	public CommandExecutor(IntelliCommand intelliCommand)
	{
		this.intelliCommand = Objects.requireNonNull(intelliCommand, "intelliCommand");
		this.commandParser = intelliCommand.commandParserFactory().build();
	}
	
	/**
	 * performs a command execution attempt on every command registered with
	 * IntelliCommand using the provided commandString and Bundle
	 * 
	 * @param commandString
	 *            to match into a command to execute
	 * @param bundle
	 *            to pass along to the command
	 * @return a {@link CommandExecutionReport} providing information about the
	 *         command execution
	 * @throws UnderlyingCommandException
	 *             if the executed command threw an exception
	 * @throws CommandNotFoundException
	 *             if no matching commands were found
	 * @throws CommandArgumentMismatchException
	 *             if 1 or more matching commands were found, but arguments did
	 *             not match
	 * @throws MalformedCommandException
	 *             if the arguments were broken, such as by a non-closed quote
	 */
	public CommandExecutionReport execute(String commandString, Bundle bundle) throws UnderlyingCommandException, CommandNotFoundException, CommandArgumentMismatchException, MalformedCommandException
	{
		Objects.requireNonNull(commandString, "commandString");
		Objects.requireNonNull(bundle, "bundle");
		
		List<String> args = StringSplitBuilder.perform(commandString);
		
		for (Command command : intelliCommand.commands())
		{
			try
			{
				return executeCommand(command, bundle, args);
			}
			catch (CommandRejectedException e)
			{
				// ignore command
			}
		}
		if (mismatchingArgumentCommands.isEmpty())
		{
			throw new CommandNotFoundException(commandString);
		}
		else
		{
			throw new CommandArgumentMismatchException(commandString, mismatchingArgumentCommands);
		}
	}
	
	/**
	 * attempts to execute a specific Command using provided bundle and args
	 * will throw a CommandRejectedException
	 * 
	 * @param command
	 *            to attempt to execute
	 * @param bundle
	 *            to pass along
	 * @param args
	 *            to run command with
	 * @return a {@link CommandExecutionReport} providing information about the
	 *         command execution
	 * @throws UnderlyingCommandException
	 *             if the executed command threw an exception
	 * @throws CommandRejectedException
	 *             if the parser rejected the arguments for this command
	 */
	private CommandExecutionReport executeCommand(Command command, Bundle bundle, List<String> args) throws UnderlyingCommandException, CommandRejectedException
	{
		MethodExpecations expectations = commandParser.parse(command, args, intelliCommand.mappers(), bundle);
		
		mismatchingArgumentCommands.add(command);
		
		if (expectations.isMet() == false)
		{
			throw new CommandRejectedException();
		}
		
		Object[] arguments = expectations.generateArguments();
		
		intelliCommand.interceptors().intercept(bundle, command, arguments);
		
		long time = System.currentTimeMillis();
		Object returnValue = command.execute(arguments);
		time = (System.currentTimeMillis() - time);
		return new CommandExecutionReport(command, arguments, (int) time, returnValue);
	}
}
