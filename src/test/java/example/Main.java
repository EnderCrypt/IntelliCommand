/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package example;


import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;
import endercrypt.library.intellicommand.execution.CommandExecutionReport;

import java.util.Scanner;
import java.util.stream.Collectors;


public class Main
{
	private static Scanner scanner = new Scanner(System.in);
	private static IntelliCommand intelliCommand = new IntelliCommand();
	
	private static int count = 0;
	
	public static void main(String[] args)
	{
		intelliCommand.commands().register(new TestingCommands());
		
		while (true)
		{
			System.out.print("> ");
			String text = scanner.nextLine();
			
			count++;
			
			Bundle bundle = Bundle.createWith()
				.instance("count", count)
				.getBundle();
			
			try
			{
				CommandExecutionReport report = intelliCommand.execute(text, bundle);
				
				Command command = report.getCommand();
				System.out.println("you ran the command: " + command.getPrimarySignature().asText() + "\n\"" + command.getInformation().getDescription().orElse("[no description]") + "\"");
			}
			catch (MalformedCommandException e)
			{
				System.out.println(e.getMessage());
			}
			catch (CommandArgumentMismatchException e)
			{
				System.out.println("did you mean?\n\t - " + e.getCommands().stream().map(c -> c.getPrimarySignature().asText()).collect(Collectors.joining("\n\t - ")));
			}
			catch (CommandNotFoundException e)
			{
				System.out.println("Command not found");
			}
			catch (UnderlyingCommandException e)
			{
				e.getCause().printStackTrace();
			}
		}
	}
}
