/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package example;


import endercrypt.library.intellicommand.annotation.CommandInformation;
import endercrypt.library.intellicommand.annotation.CommandParam;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Leftovers;


@CommandSignature("")
public class TestingCommands
{
	@CommandInformation(category = "f", description = "fgdgdfg")
	@CommandSignature("hello {arg}")
	public void hello(@CommandParam("arg") String text)
	{
		System.out.println("said: " + text);
	}
	
	@CommandSignature("inf {arg}")
	@CommandInformation(description = "hey")
	public void inf(@CommandParam("arg") String text, @Leftovers String leftovers)
	{
		System.out.println("arg: " + text + ", leftovers: \"" + leftovers + "\"");
	}
	
	@CommandSignature("int {arg}")
	public void inf(@CommandParam("arg") int num)
	{
		System.out.println("number: " + num);
	}
	
	@CommandSignature("1")
	public void one()
	{
		System.out.println("1");
	}
	
	@CommandSignature("1 1")
	public void oneOne()
	{
		System.out.println("1 1");
	}
	
	@CommandSignature("2")
	public void two(@Leftovers String leftover)
	{
		System.out.println("2: " + leftover);
	}
	
	@CommandSignature("2 2")
	public void twoTwo()
	{
		System.out.println("2 2");
	}
	
	@CommandSignature("count")
	public void countCommands(@Include("count") int count)
	{
		System.out.println("you have ran " + count + " commands");
	}
}
