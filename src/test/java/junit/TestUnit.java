/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package junit;


import static org.junit.jupiter.api.Assertions.*;

import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.command.Command;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandExecutionException;
import endercrypt.library.intellicommand.exception.execution.InterceptorException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.interceptor.Interceptor;

import java.awt.Point;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import junit.relevant.Food;
import junit.relevant.ValueEnum;


class TestUnit
{
	private static Random random;
	private static IntelliCommand intelliCommand;
	private static TestUnitCommands testUnitCommands;
	
	private Bundle bundle;
	
	@BeforeAll
	public static void beforeClass()
	{
		random = new Random();
		
		intelliCommand = new IntelliCommand();
		intelliCommand.interceptors().register(new Interceptor()
		{
			@Override
			public void intercept(Bundle bundle, Command command, Object[] arguments)
			{
				if (bundle.has("intercept"))
				{
					bundle.attach()
						.instance("intercept_tostring", bundle.get("intercept").supply().toString());
				}
				
				if (bundle.has("interrupt") && (Boolean) bundle.get("interrupt").supply())
				{
					throw new IllegalArgumentException();
				}
			}
		});
		
		testUnitCommands = new TestUnitCommands();
		
		intelliCommand.commands().register(testUnitCommands);
	}
	
	@BeforeEach
	public void before()
	{
		bundle = new Bundle();
	}
	
	@AfterEach
	public void after()
	{
		testUnitCommands.clearResults();
	}
	
	@Test
	void canExecuteBasicCommand1() throws CommandExecutionException
	{
		intelliCommand.execute("test empty1", bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("empty1");
		assertEquals(true, results.get("empty1"));
	}
	
	// EMPTY //
	
	@Test
	void empty1() throws CommandExecutionException
	{
		intelliCommand.execute("test empty1", bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("empty1");
		assertEquals(true, results.get("empty1"));
	}
	
	// BASIC //
	
	@Test
	void basic1() throws CommandExecutionException
	{
		int number = random.nextInt(1_000_000);
		
		intelliCommand.execute("test basic1 " + number, bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("basic1");
		assertEquals(number, results.get("basic1"));
	}
	
	@Test
	void basic2() throws CommandExecutionException
	{
		String text = new String(random.ints(10, 97, 122).toArray(), 0, 10);
		
		intelliCommand.execute("test basic2 " + text, bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("basic2");
		assertEquals(text, results.get("basic2"));
	}
	
	@Test
	void basic3() throws CommandExecutionException
	{
		int number = random.nextInt(1_000_000);
		String text = new String(random.ints(10, 97, 122).toArray(), 0, 10);
		
		intelliCommand.execute("test basic3 " + number + " " + text, bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("basic3_int", "basic3_string");
		assertEquals(number, results.get("basic3_int"));
		assertEquals(text, results.get("basic3_string"));
	}
	
	// INTERCEPT //
	
	@Test
	void intercept_modify_bundle() throws CommandExecutionException
	{
		int[] array = new int[0];
		
		bundle.attach()
			.instance("intercept", array);
		
		intelliCommand.execute("test intercept none", bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("intercept none");
		assertEquals(array.toString(), bundle.get("intercept_tostring").supply());
		assertEquals("yes", results.get("intercept none"));
	}
	
	@Test
	void intercept_interrupt()
	{
		bundle.attach()
			.instance("interrupt", true);
		
		assertThrows(InterceptorException.class, () -> intelliCommand.execute("test intercept none", bundle));
		
		assertFalse(testUnitCommands.hasResult("intercept none"));
	}
	
	// PRIMITIVES //
	
	@Test
	void primitives1_int() throws CommandExecutionException
	{
		int num1 = random.nextInt(1_000_000);
		int num2 = random.nextInt(1_000_000);
		
		intelliCommand.execute("test primitives1 " + num1 + " " + num2, bundle);
		
		Map<String, Object> results = testUnitCommands.getResult("primitives1_int", "primitives1_integer");
		assertEquals(num1, results.get("primitives1_int"));
		assertEquals(num2, results.get("primitives1_integer"));
	}
	
	// OVERLOAD //
	
	@Test
	void overload1_int() throws CommandExecutionException
	{
		int number = random.nextInt(1_000_000);
		
		intelliCommand.execute("test overload1 " + number, bundle);
		
		assertEquals(number, testUnitCommands.getResult("overload1_int").get("overload1_int"));
	}
	
	@Test
	void overload1_string() throws CommandExecutionException
	{
		String text = new String(random.ints(10, 97, 122).toArray(), 0, 10);
		
		intelliCommand.execute("test overload1 " + text, bundle);
		
		assertEquals(text, testUnitCommands.getResult("overload1_string").get("overload1_string"));
	}
	
	@Test
	void overload1_boolean() throws CommandExecutionException
	{
		boolean bool = random.nextBoolean();
		
		intelliCommand.execute("test overload1 " + bool, bundle);
		
		assertEquals(bool, testUnitCommands.getResult("overload1_boolean").get("overload1_boolean"));
	}
	
	// MISSING //
	
	@Test
	void missing_command()
	{
		assertThrows(CommandArgumentMismatchException.class, () -> intelliCommand.execute("test missing1 yukgvdcdthsfaftj", bundle));;
	}
	
	@Test
	void missing_1()
	{
		assertThrows(CommandArgumentMismatchException.class, () -> intelliCommand.execute("test missing1 test", bundle));
	}
	
	// QUOTES //
	
	@Test
	void quote_1()
	{
		assertThrows(MalformedCommandException.class, () -> intelliCommand.execute("test \"quote1", bundle));
	}
	
	@Test
	void quote_2() throws CommandExecutionException
	{
		String text = "hello world";
		intelliCommand.execute("test quote2 \"" + text + "\"", bundle);
		
		assertEquals(text, testUnitCommands.getResult("quote2").get("quote2"));
	}
	
	// BUNDLE //
	
	@Test
	void bundle_1() throws CommandExecutionException
	{
		Point point1 = new Point(1, 2);
		Point point2 = new Point(3, 4);
		
		bundle.attach()
			.instance("item1", point1)
			.instance("item2", point2);
		
		intelliCommand.execute("test bundle1", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("point1", "point2");
		
		assertSame(point1, result.get("point1"));
		assertSame(point2, result.get("point2"));
	}
	
	// infinity argument
	
	@Test
	void infinity_1() throws CommandExecutionException
	{
		intelliCommand.execute("test infinity 1", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("infinity_1");
		
		assertEquals("1", result.get("infinity_1"));
	}
	
	@Test
	void infinity_2() throws CommandExecutionException
	{
		intelliCommand.execute("test infinity 1", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("infinity_1");
		
		assertEquals("1", result.get("infinity_1"));
	}
	
	@Test
	void infinity_3() throws CommandExecutionException
	{
		intelliCommand.execute("test infinity k", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("infinity_1");
		
		assertEquals("k", result.get("infinity_1"));
	}
	
	@Test
	void infinity_4() throws CommandExecutionException
	{
		intelliCommand.execute("test infinity what lol", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("infinity_1");
		
		assertEquals("what lol", result.get("infinity_1"));
	}
	
	@Test
	void infinity_5() throws CommandExecutionException
	{
		intelliCommand.execute("test infinity key  lol", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("infinity_1");
		
		assertEquals("key lol", result.get("infinity_1"));
	}
	
	@Test
	void infinity_2_1() throws CommandExecutionException
	{
		intelliCommand.execute("test infinity_2 342", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("infinity_2");
		
		assertEquals(342, result.get("infinity_2"));
	}
	
	// leftover overflow check
	
	@Test
	void leftover_1() throws CommandExecutionException
	{
		intelliCommand.execute("test leftover", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("leftover");
		
		assertEquals("1", result.get("leftover"));
	}
	
	@Test
	void leftover_2() throws CommandExecutionException
	{
		intelliCommand.execute("test leftover leftover", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("leftover");
		
		assertEquals("2", result.get("leftover"));
	}
	
	// aliases
	
	@Test
	void alias_1() throws CommandExecutionException
	{
		intelliCommand.execute("test alias1 second", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("alias");
		
		assertEquals("1", result.get("alias"));
	}
	
	@Test
	void alias_2() throws CommandExecutionException
	{
		intelliCommand.execute("test alias3 4", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("alias");
		
		assertEquals("2", result.get("alias"));
	}
	
	// enums
	
	@Test
	void enum_1() throws CommandExecutionException
	{
		intelliCommand.execute("test enum1 value_2", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("enum");
		
		assertEquals(ValueEnum.VALUE_2, result.get("enum"));
	}
	
	// subclass
	
	@Test
	void subclass_1() throws CommandExecutionException
	{
		intelliCommand.execute("test subclass1 32", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("subclass");
		
		Number number = (Number) result.get("subclass");
		assertEquals(32, number.intValue());
	}
	
	// abort mapper
	
	@Test
	void abort_1() throws CommandExecutionException
	{
		intelliCommand.execute("test abort fish", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("abort");
		
		assertEquals(Food.FISH, result.get("abort"));
	}
	
	@Test
	void abort_2() throws CommandExecutionException
	{
		intelliCommand.mappers().register(Food.class, new Food.Mapper());
		intelliCommand.execute("test abort noodles", bundle);
		
		Map<String, Object> result = testUnitCommands.getResult("abort");
		
		assertEquals(Food.NOODLES, result.get("abort"));
	}
	
	@Test
	void abort_3()
	{
		intelliCommand.mappers().register(Food.class, new Food.Mapper());
		assertThrows(CommandArgumentMismatchException.class, () -> intelliCommand.execute("test abort fish", bundle));
	}
	
	// automatic name
	
	void automatic() throws CommandExecutionException
	{
		intelliCommand.execute("test automatic success");
		assertEquals("success", testUnitCommands.getResult("automatic"));
	}
}
