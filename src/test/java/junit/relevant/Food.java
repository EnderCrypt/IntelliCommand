/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package junit.relevant;


import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.mapper.ArgumentMapper;
import endercrypt.library.intellicommand.mapper.MapperConversionAbort;
import endercrypt.library.intellicommand.mapper.MapperConversionFail;

import java.util.Arrays;


public enum Food
{
	NOODLES(true),
	FISH(false),
	HAMBURGER(true);
	
	private final boolean good;
	
	private Food(boolean good)
	{
		this.good = good;
	}
	
	public boolean isGood()
	{
		return good;
	}
	
	public static class Mapper implements ArgumentMapper<Food>
	{
		@Override
		public Food map(Bundle bundle, Class<? extends Food> targetClass, String text) throws MapperConversionFail, MapperConversionAbort
		{
			return Arrays.stream(values())
				.filter(Food::isGood)
				.filter(f -> f.toString().equalsIgnoreCase(text))
				.findFirst().orElseThrow(MapperConversionAbort::new);
		}
	}
}
