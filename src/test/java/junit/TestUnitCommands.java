/************************************************************************
 * IntelliCommand by EnderCrypt                                         *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package junit;


import endercrypt.library.intellicommand.annotation.CommandAlias;
import endercrypt.library.intellicommand.annotation.CommandParam;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Leftovers;
import endercrypt.library.intellicommand.annotation.Priority;

import java.awt.Point;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.relevant.Food;
import junit.relevant.ValueEnum;


@CommandSignature("test")
public class TestUnitCommands
{
	private Map<String, Object> testResults = new HashMap<>();
	
	public Map<String, Object> getResult(String... keys)
	{
		if (hasResult(keys))
		{
			return Collections.unmodifiableMap(testResults);
		}
		else
		{
			throw new IllegalArgumentException("test results contained the wrong keys (contained: " + testResults.keySet() + ")");
		}
	}
	
	public boolean hasResult(String... keys)
	{
		Set<String> testKeyList = testResults.keySet();
		List<String> keysList = Arrays.asList(keys);
		
		return (testResults.keySet().containsAll(keysList) && keysList.containsAll(testKeyList));
	}
	
	public void clearResults()
	{
		testResults.clear();
	}
	
	// EMPTY //
	@CommandSignature("empty1")
	public void commandEmpty1()
	{
		testResults.put("empty1", true);
	}
	
	// BASIC //
	
	@CommandSignature("basic1 {var1}")
	public void commandBasic1(@CommandParam("var1") int number)
	{
		testResults.put("basic1", number);
	}
	
	@CommandSignature("basic2 {var1}")
	public void commandBasic2(@CommandParam("var1") String text)
	{
		testResults.put("basic2", text);
	}
	
	@CommandSignature("basic3 {var1} {var2}")
	public void commandBasic2(@CommandParam("var1") int num, @CommandParam("var2") String text)
	{
		testResults.put("basic3_int", num);
		testResults.put("basic3_string", text);
	}
	
	// INTERCEPT //
	
	@CommandSignature("intercept none")
	public void intercept_modify_bundle()
	{
		testResults.put("intercept none", "yes");
	}
	
	// PRIMITIVES //
	
	@CommandSignature("primitives1 {var1} {var2}")
	public void primitives1(@CommandParam("var1") Integer num1, @CommandParam("var2") int num2)
	{
		testResults.put("primitives1_int", num1);
		testResults.put("primitives1_integer", num2);
	}
	
	// OVERLOAD //
	
	@Priority(2)
	@CommandSignature("overload1 {var1}")
	public void oveload1_int(@CommandParam("var1") int num)
	{
		testResults.put("overload1_int", num);
	}
	
	@Priority(1)
	@CommandSignature("overload1 {var1}")
	public void overload1_boolean(@CommandParam("var1") boolean bool)
	{
		testResults.put("overload1_boolean", bool);
	}
	
	@CommandSignature("overload1 {var1}")
	public void overload1_string(@CommandParam("var1") String text)
	{
		testResults.put("overload1_string", text);
	}
	
	// MISSING //
	
	@CommandSignature("missing1 {var1}")
	public void missing_1(@CommandParam("var1") int number)
	{
		testResults.put("missing1", number);
	}
	
	// QUOTE //
	
	@CommandSignature("quote1 {var1}")
	public void quote_1(@CommandParam("var1") String text)
	{
		testResults.put("quote1", text);
	}
	
	@CommandSignature("quote2 {var1}")
	public void quote_2(@CommandParam("var1") String text)
	{
		testResults.put("quote2", text);
	}
	
	// BUNDLE //
	
	@CommandSignature("bundle1")
	public void bundle_1(@Include("item1") Point point1, @Include("item2") Point point2)
	{
		testResults.put("point1", point1);
		testResults.put("point2", point2);
	}
	
	// infinity
	
	@CommandSignature("infinity")
	public void infinity_1(@Leftovers String leftover)
	{
		testResults.put("infinity_1", leftover);
	}
	
	@CommandSignature("infinity_2")
	public void infinity_2(@Leftovers int leftover)
	{
		testResults.put("infinity_2", leftover);
	}
	
	// leftover overflow check
	
	@CommandSignature("leftover")
	public void leftoverOne()
	{
		testResults.put("leftover", "1");
	}
	
	@CommandSignature("leftover leftover")
	public void leftoverTwo()
	{
		testResults.put("leftover", "2");
	}
	
	// alias
	
	@CommandSignature("alias1")
	@CommandAlias({ "alias1 second" })
	public void aliasOne()
	{
		testResults.put("alias", "1");
	}
	
	@CommandSignature("alias2")
	@CommandAlias({ "alias2", "alias2 2", "alias3 4" })
	public void aliasTwo()
	{
		testResults.put("alias", "2");
	}
	
	// enum
	@CommandSignature("enum1 {sample}")
	public void enum1(@CommandParam("sample") ValueEnum sample)
	{
		testResults.put("enum", sample);
	}
	
	// subclass
	@CommandSignature("subclass1 {num}")
	public void subclass1(@CommandParam("num") Number number)
	{
		testResults.put("subclass", number);
	}
	
	// abort
	@CommandSignature("abort {enum}")
	public void abort(@CommandParam("enum") Food food)
	{
		testResults.put("abort", food);
	}
	
	@CommandSignature("automatic {auto}")
	public void automatic(@CommandParam String auto)
	{
		testResults.put("automatic", auto);
	}
}
