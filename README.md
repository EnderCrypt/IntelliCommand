# IntelliCommand

Library inspired by the design of the Jax-RS (a framework of setting up REST endpoints) but instead using the design pattern for executing commands.

Commands as those who may be executed in programs such as chats, terminals, game debug consoles and so on.

This library makes it easy to setup your commands quickly, along with the command prefix, command araguments, automatic conversion from textual arguments to java objects (including the ability to add your own conversions).

This library is also incredibly lightweight, requiring 0 dependancies (with JUnit for unit tests to ensure quality and stability of this library)

JitPack: [![](https://jitpack.io/v/com.gitlab.EnderCrypt/IntelliCommand.svg)](https://jitpack.io/#com.gitlab.EnderCrypt/IntelliCommand)

Maven:
```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>

<dependancies>
    <dependency>
        <groupId>com.gitlab.EnderCrypt</groupId>
        <artifactId>IntelliCommand</artifactId>
        <version>VERSION</version>
    </dependency>
</dependancies>
```

Version should be set to a commit hash, it is recommended to use the latest one from the master branch for stability.